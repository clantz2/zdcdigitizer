## ZDC Digitizer

Everything is super simple right now. We can complicate it more if we want.

I made a single .cc file with three functions:

___

#### cutZDCoutput(string fileName, string rod, float xOffset = 0, float zOffset = 0)

  - ##### Arguments:

    - fileName - The input .root file

    - rod - The transmission spectrum you would like to use. Options are 1,2,3a,3b,4,5,6, and fusedQuartz.

    - xOffset - The distance to offset the data in x

    - zOffset - The distance to offset the data in z

  - ##### Description:
    - Takes an input .root file with photon data from the top of the ZDC and cuts it based on the transmission spectrum for the selected rod.


  - ##### Output:
    - .root file and .txt file with photon data after cuts. Named the same as the input file prepended with Out_

___

#### PMTcuts(string fileName, int PMTmodel = 6091)
  - ##### Arguments:
    - fileName - The input .root file
    - PMTmodel - Loads the Quantum efficiency curve for the given PMT model #. Currently we only have data for the 6091. Waiting on datasheets from Peter for the other options.

  - ##### Description:
    - Takes an input .root file with photon data from the top of the ZDC light guide
    and cuts it based on the Quantum Efficiency for the selected PMT model.

  - ##### Output:
    - .txt file with photon data after cuts. Named the same as the input file
    prepended with Out_

___

### Running it

For now we just load it in root and execute the funtions. No compiling.

```
root[0] .L outputDigitizer.cc

root[1] cutZDCoutput("testlight.root","3a",0,-100)
```

or

```
root[0] .L outputDigitizer.cc

root[1] PMTcuts("test.root",6091)
```
